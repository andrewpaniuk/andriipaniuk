    $(".typed").typed({
        strings: ["Добро пожаловать на мой сайт", "Меня зовут А.Панюк", "Я Front-End Developer", "Люблю простоту"],
        typeSpeed: 100,
        backDelay: 900,
        loop: true
    });

    $(".typed2").typed({
        strings: ["Welcome to my Website", "My name is A.Paniuk", "I am Front-End Developer", "Love Simplicity"],
        typeSpeed: 100,
        backDelay: 900,
        loop: true
    });


    var stickyNavTop = $('#header').offset().top;

    $(window).scroll(function() {
        if ($(this).scrollTop() > stickyNavTop) {
            $('#header').addClass('fixed-menu');
        } else {
            $('#header').removeClass('fixed-menu');
        }
    });




        $('.slide-one').owlCarousel({
          items: 1,
            loop: false,
            dots: true,
        })



        //
        $('.slide-two').owlCarousel({
            items: 7,
            loop: true,
            autoplay: true,
            autoplayTimeout: 2000,
            nav: true,
            dots: false,
            navText: [""],
            responsive: {
                0: {
                    items: 3
                },
                600: {
                    items: 4
                },
                1000: {
                    items: 7
                }
            }
        });



    // Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });

  $(document).ready(function() {
      $('#submit').click(function() {
          $('#messageShow').hide ();
          var name = $('#name').val ();
          var email = $('#email').val ();
          var message = $('#message').val ();
          var fail = '';
          if (name.length < 3) fail = 'Please enter your name with at least 3 characters';
          else if (email.split ('@').length - 1 == 0 || email.split('.').length -1 == 0)
          fail = "You entered an incorrect email";
          else if (message.length < 10)
          fail = "Please enter a message with at least 10 characters";
          if (fail !='') {
              $('#messageShow').html (fail);
               $('#messageShow').show ();
               return false;
          }
          $.ajax ({
              url: '../email.php',
              type: 'POST',
              cache: false,
              data: {'name': name, 'email': email, 'message': message},
              dataType: 'html',
              success: function(data) {
                $('#messageShow').html (data);
                $('#messageShow').show ();
              }
          });

      });
  });

  $(document).ready(function() {
      jQuery('.skillbar').each(function() {
          jQuery(this).appear(function() {
              jQuery(this).find('.count-bar').animate({
                  width: jQuery(this).attr('data-percent')
              }, 3000);
              var percent = jQuery(this).attr('data-percent');
              jQuery(this).find('.count').html('<span>' + percent + '</span>');
          });
      });
  });
