<?php
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];

    $to = "admin@andriipaniuk.ml";
    $subject = "Письмо от посетителя";

    $body = "Вы получили письмо от " . $name . " (" . $email . "):\n\n" . $message;

    $headers = 'From: admin@andriipaniuk.ml' . "\r\n";

    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if (mail ($to, $subject, $body, $headers)) {
                echo 'Thank you! Your message has been sent.<br>
                <a href="/index.html">Home page >>></a>';
            } else {
                echo 'Something went wrong ... Try again!<br>
                <a href="/index.html">Home page >>></a>';
            }
        }
